package com.twuc.webApp.contract;

import com.twuc.webApp.domain.UserEntity;

public class GetUserResponse {
    private String firstName;
    private String lastName;

    public GetUserResponse(UserEntity entity) {
        firstName = entity.getFirstName();
        lastName = entity.getLastName();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
