package com.twuc.webApp.domain;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {
    private List<UserEntity> storage = Arrays.asList(
        new UserEntity(1L, "Uzumaki", "Naruto"),
        new UserEntity(2L, "Edogawa", "Conan"),
        new UserEntity(3L, "Sinnjou", "Akane")
    );

    public Optional<UserEntity> findById(Long id) {
        return storage.stream().filter(u -> u.getId().equals(id)).findFirst();
    }
}
